package com.wipro.inventory;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import Entity.SKU;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;



@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InventoryServiceApplicationTests {

	@Autowired
	TestRestTemplate restTemplate;
	@Test
	public void test01contextLoads() {
		assertNotNull("Test Template not available", restTemplate);
		
	}

	@Test
	public void  test02GetSKUs() {
		ResponseEntity<List> res = restTemplate.getForEntity ("/skus", List.class);
		assertEquals(HttpStatus.OK, res.getStatusCode());
		assertEquals(0, res.getBody().size());
		
	}
	
	@Test
	public void test03NewSKU() {
		SKU _sku = new SKU();
		_sku.setProductId(100);
		_sku.setName("Product 1");
		_sku.setDescription("Product Descirption");
		_sku.setPrice(100.5);
		_sku.setCount(20);
		
		ResponseEntity<String> res = restTemplate.postForEntity("/skus", _sku, String.class);
		
		assertEquals(200, res.getStatusCode());

		//assertEquals(HttpStatus.CREATED, res.getStatusCode());
		System.out.println(res.getBody());
		
	}

	
}
