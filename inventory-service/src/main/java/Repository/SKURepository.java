package Repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import Entity.SKU;

@Repository
public interface SKURepository extends CrudRepository<SKU, Long> {
	
	SKU findById(Integer id);
	
}
