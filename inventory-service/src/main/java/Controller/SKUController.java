package Controller;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import Entity.SKU;
import Entity.SKUPayload;
import Repository.SKURepository;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class SKUController {
	
	@Autowired 
	SKURepository repository;
	
	@GetMapping("/skus")
	public List<SKU> getSKU() {
		return Collections.emptyList();
	}
	
	@PostMapping("/skus")
	public ResponseEntity<Object> newSKU (@RequestBody SKUPayload skuPayload)  throws IOException {	
		HttpHeaders headers = new HttpHeaders();
		if(skuPayload.getProductId() == null
				|| skuPayload.getPrice() == null
				|| skuPayload.getCount() == null) {
			headers.set("Content-Type", "text/plain");
			return new ResponseEntity<>("productId is mandatory",headers,HttpStatus.BAD_REQUEST);
		}
		else {
			SKU sku = new SKU();
			sku.setCount(skuPayload.getCount());
			sku.setDescription(skuPayload.getDescription());
			sku.setName(skuPayload.getName());
			sku.setPrice(skuPayload.getPrice());
			sku.setProductId(skuPayload.getProductId());
			SKU success = repository.save(sku);
			if(success==null) {
				headers.set("Content-Type", "application/json");
				return new ResponseEntity<>("productId is mandate",headers,HttpStatus.BAD_REQUEST);
			}
			else {
				headers.set("Content-Type", "application/json");
				return new ResponseEntity<>(success,headers,HttpStatus.CREATED);
			}		
		}
		
	}
}
